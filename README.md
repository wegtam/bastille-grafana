# Grafana template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[Grafana](https://grafana.com/) service inside of a
[FreeBSD](https://www.freebsd.org/) jail.

Please note that `$CONF_DIR` and `$DATA_DIR` directories will be created
inside the jail and mounted from their external counterparts which are
expected to be existing. Also the rendered configuration is copied to the
`$CONF_DIR` if no `grafana.ini` is already existing there.

Remember that you'll need to allow the port to be queried from outside the
jail depending on your setup.

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

```
# bastille boostrap https://codeberg.org/wegtam/bastille-grafana.git
```

## Usage

### 1. Install with default settings

```
# bastille template TARGET wegtam/bastille-grafana
```

### 2. Install with custom settings

```
# bastille template grafana wegtam/bastille-grafana \
  --arg CONF_DIR=/srv/grafana/conf --arg DATA_DIR=/srv/grafana/data
```

